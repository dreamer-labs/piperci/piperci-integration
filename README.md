# PiperCI-Integration

A repository to run integration tests of PiperCI using GitLab CI.

This repository contains CI jobs which are used by upstream pipelines to test component changes in a full PiperCI environment. 


## Variables

The configuration of the jobs in this repository are controlled by variables. Currently there is one variable which can be set to control
the type of PiperCI installation that is used.

### TYPE

The *TYPE* variable, when set to *functional*, will deploy PiperCI in a Gitlab DIND environment. The purpose of this installation is to provide
a very rapid installation of PiperCI, so that component changes can be tested quickly without incurring too much slowdown to the upstream component pipeliens.

## Configuration

To configure an upstream pipelien to use this repository add the following job:

```
downstream:
  variables:
    gman: "{'gman': {'image': \'$CI_REGISTRY_IMAGE\', 'tag': \'$CI_COMMIT_REF_SLUG\'}}"
    TYPE: functional
  stage: integration
  only:
    - merge_requests
    - master
  trigger:
    project: dreamer-labs/piperci/piperci-integration
    strategy: depend
```

The `variables` listed here are specific to the installer repository. Any variable defined here will be passed to the environment of the PiperCI-Installer which can then be used by the Ansible jobs which are run to deploy the infrastructure. The above example provides an override for Gman which will set the image and tag used by the Gman Ansible job. After the downstream job is called we will wait for the results of the piperci-integration pipeline before returning successfully. This is accomplished using the `stategy: depend` flag in the bridge job. 
